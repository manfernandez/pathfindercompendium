package com.jellware.pfcompendium.data.database;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Jellinek on 27/12/13.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "pfdb.db";
    private SQLiteDatabase db;
    private final Context context;
    private String DB_PATH;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
        DB_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";
    }

    //Returns TRUE when database is created. FALSE if any exception
    public boolean createDataBase() {

        boolean dbExist = checkDataBase();
        //Check for update

        if (dbExist) {
            try{
                db = SQLiteDatabase.openDatabase( DB_PATH + DB_NAME, null,SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);

            }catch(Exception e){
                return false;
            }
            return true;
        } else {

            this.getReadableDatabase();
            try {

                copyDataBase();

                return true;

            } catch (Exception e) {
                return false;
            }
        }
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() throws IOException {

        //First, if update is available, delete old database

        InputStream myInput = null;

        myInput = context.getAssets().open(DB_NAME);


        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

        //Open database after load
        db = SQLiteDatabase.openDatabase( DB_PATH + DB_NAME, null,SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);


    }




    @Override
    public void onCreate(SQLiteDatabase arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion>oldVersion){
            try{
                copyDataBase();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public SQLiteDatabase getDb() {
        return db;
    }
}