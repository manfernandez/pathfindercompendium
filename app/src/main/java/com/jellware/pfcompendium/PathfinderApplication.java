package com.jellware.pfcompendium;

import android.app.Application;

import com.jellware.pfcompendium.controller.Controller;

public class PathfinderApplication extends Application {

    public static String packageName;

    @Override
    public void onCreate() {
        super.onCreate();
        packageName = getApplicationContext().getPackageName();
        Controller.getInstance(getApplicationContext());

    }
}
