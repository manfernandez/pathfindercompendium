package com.jellware.pfcompendium.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jellware.pfcompendium.R;

public class FragmentMonsterFilter extends BaseFilterFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.filter_monsters, container, false);

        return rootView;
    }


}
