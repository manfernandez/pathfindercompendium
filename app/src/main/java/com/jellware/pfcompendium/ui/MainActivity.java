package com.jellware.pfcompendium.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.jellware.pfcompendium.R;
import com.jellware.pfcompendium.controller.constants.IntentConstants;
import com.jellware.pfcompendium.controller.enums.CategoryEnum;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private LinearLayout layCatMonster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layCatMonster = findViewById(R.id.layCatMonster);
        layCatMonster.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.equals(layCatMonster)){
            Intent intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra(IntentConstants.INTENT_CATEGORY, CategoryEnum.monsters);
            startActivity(intent);
        }

    }
}
