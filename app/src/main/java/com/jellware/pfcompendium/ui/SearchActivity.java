package com.jellware.pfcompendium.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.widget.TextView;

import com.jellware.pfcompendium.R;
import com.jellware.pfcompendium.controller.Controller;
import com.jellware.pfcompendium.controller.constants.IntentConstants;
import com.jellware.pfcompendium.controller.enums.CategoryEnum;
import com.jellware.pfcompendium.ui.fragments.BaseFilterFragment;
import com.jellware.pfcompendium.ui.fragments.FragmentListView;
import com.jellware.pfcompendium.ui.fragments.FragmentMonsterFilter;

public class SearchActivity extends AppCompatActivity {

    private CategoryEnum category;

    //Views
    private AppCompatImageView imgCategorySearch, imgFilter;
    private TextView txtCategorySearch;
    private ViewPager pager;

    private FragmentFilterSlideAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        if(getIntent().getExtras() != null) {
            category = (CategoryEnum) getIntent().getSerializableExtra(IntentConstants.INTENT_CATEGORY);
        }
        //Load Views - Category Search
        imgCategorySearch = findViewById(R.id.imgCategorySearch);
        txtCategorySearch = findViewById(R.id.txtCategorySearch);
        imgFilter = findViewById(R.id.btnCategorySearchFilter);
        pager = findViewById(R.id.pager);


        switch (category){
            case monsters:
                imgCategorySearch.setImageResource(R.drawable.cat_monsters);
                txtCategorySearch.setText("Monsters");
                break;
        }

        adapter = new FragmentFilterSlideAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

        Controller.getInstance(this).getMonsterController().getMonstersByStringName("abol");

        //OnClicks

    }



    public void onBackPressed() {
        if (pager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }



    private class FragmentFilterSlideAdapter extends FragmentStatePagerAdapter {

        BaseFilterFragment filterFragment;
        FragmentListView fragmentListView;

        public FragmentFilterSlideAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
                fragmentListView = new FragmentListView();
                return fragmentListView;
            }
            else if (position == 1){
                switch (category){
                    case monsters:
                        filterFragment = new FragmentMonsterFilter();
                        break;
                }
                return filterFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }



}
