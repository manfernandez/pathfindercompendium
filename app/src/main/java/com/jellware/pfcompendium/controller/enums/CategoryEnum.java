package com.jellware.pfcompendium.controller.enums;

public enum CategoryEnum {

    all,
    classes,
    feats,
    spells,
    races,
    items,
    monsters
}
