package com.jellware.pfcompendium.controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.jellware.pfcompendium.controller.categorycontrollers.MonsterController;
import com.jellware.pfcompendium.data.database.DatabaseHelper;

import java.io.IOException;

public class Controller {

    private static Controller singleton;
    private DatabaseHelper helper;
    private SQLiteDatabase database;

    private MonsterController monsterController;

    public static Controller getInstance(Context context){
        if(singleton == null)
            singleton = new Controller(context);
        return singleton;
    }

    private Controller(Context context){


        //Init database
        helper = new DatabaseHelper(context.getApplicationContext());

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                helper.createDataBase();
                monsterController = new MonsterController(helper.getDb());

            }
        };
        AsyncTask.execute(runnable);


    }

    public DatabaseHelper getDatabase(Context context){
        return helper;
    }

    public MonsterController getMonsterController(){
        return monsterController;
    }
}
